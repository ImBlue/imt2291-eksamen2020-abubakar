<?php

class User{

    protected $db;

    public function __construct($db){
        $this->db = $db;
    }
    
    /**
     * Sees if the email already exists in the database
     */
    public function checkEpost($epost) {
		$sql = 'SELECT uname FROM user WHERE uname = ?';
		$sth = $this->db->prepare($sql);
		$sth->execute(array($epost));
		
		$tmp = array();
		if ($sth->rowCount()==1) {
            $tmp['status'] = 'FAIL'; 
            $tmp['errorMsg'] = 'Epost finnes allerede i databasen';
		}
		else {
			$tmp['status'] = 'SUCCESS';
		}
		return $tmp;
    }

    /**
     * Tries to add a new user to the database
     * @param data An array of the data inputed into the registration form
     */
    public function addUser($data){
        $sql = 'INSERT INTO user (uname, pwd, firstName, lastName) VALUES(?,?,?,?)';
        $response = array();
        try{
            $stm = $this->db->prepare($sql);
            $stm->execute(array($data['e-post'], password_hash($data['passord'], PASSWORD_DEFAULT), $data['fornavn'], $data['etternavn']));
            $response['message'] = 'Ny bruker med data (Fornavn: ' . $data['fornavn'] . ', Etternavn: ' . $data['etternavn'] . '), ble lagt til databasen';
        }catch(PDOException $e){
            $response['message'] = 'Noe gikk galt med database forbindelsen. Feilmelding: ' . $e;
        }
        return $response;
    }
}