
let liste = document.getElementById("brukerListe");
let brukere = [];
let formButton = document.getElementById("form");
let rediger = document.getElementById("redigerBruker");
let respons = document.getElementById("respons");
let userid = -1;
hentBrukerListe(liste);
formButton.onclick = handleUpdate;

function hentBrukerListe(list){
    fetch("./../api/fetchUsers.php", {
        method: "GET",
        credentials: "include"
    })
    .then(res =>{
        if(res.status = "FAIL"){
            console.log(res.msg)
        }
        return res.json();
    })
    .then(res =>{
        brukere = res;
        for(let i = 0; i < brukere.length; i++){
            let listEle = document.createElement("div");
            let mail = document.createElement("p");
            let name = document.createElement("p");
            let surname = document.createElement("p");
            
            listEle.className = "listElement";
            listEle.name = brukere[i].uid;
            listEle.onclick = hentBruker;
            mail.innerHTML = brukere[i].uname;
            mail.className = "listEmail";
            name.innerHTML = brukere[i].firstName;
            name.className = "listFirstName";

            listEle.appendChild(mail);
            listEle.appendChild(name);
            listEle.appendChild(surname);
            list.appendChild(listEle);
        }
    });
}

function hentBruker(evt){
    let header = document.getElementById("endreBruker");
    let id = finnBruker(evt.target.name);
    respons.style.display = "none";
    rediger.style.display = "block";
    header.innerHTML = brukere[id].firstName + " " + brukere[id].lastName;
    userid= id;
}

function finnBruker(id){
    let result = -1;
    for(let i = 0; i < brukere.length; i++){
        if(brukere[i].uid == id){
            result = i;
        }
    }
    return result;
}

function handleUpdate(evt){
    let oldpwd = document.getElementById("oldpwd");
    let newpwd = document.getElementById("newpwd");
    let uname = document.getElementById("uname");
    let first = document.getElementById("first");
    let last = document.getElementById("last");
    let data = new FormData();
    data.append("oldpwd", oldpwd.value);
    data.append("pwd", newpwd.value);
    data.append("uname", uname.value);
    data.append("uid", userid);
    data.append("firstName", first.value);
    data.append("lastName", last.value);
    console.log(data);
    fetch("./../api/updateUser.php", {
        method: "POST",
        credentials: "include",
        body: data
    })
    .then(res => res.json())
    .then(data =>{
        if (data.status=='success') {
            respons.innerHTML = "Informasjon om brukeren er oppdatert";
            hentBrukerListe(liste);
          } else {
            respons.innerHTML = "Kunne ikke oppdatere informasjon om brukeren";
            console.log(data.msg);
          }
    })

}