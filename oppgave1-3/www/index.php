<?php
require_once "./vendor/autoload.php";
require_once "./classes/DB.php";
require_once "./classes/user.php";

$loader = new Twig_Loader_Filesystem('./templates');
$twig = new Twig_Environment($loader, array(/*'cache' => './compilation_cache',*/));

$db = DB::getDBConnection();
$user = new User($db);

if($_SERVER['REQUEST_METHOD'] == 'GET'){
    echo $twig->render('indexOppgave1.html', array('status' => 0));
}

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //Checks if the email already exists
    $emailExist = $user->checkEpost($_POST['e-post']);
    $registerData = array();
    if($emailExist['status'] == 'FAIL'){
        echo $twig->render('indexOppgave1.html', array('status' => 1, 'response' => $emailExist['errorMsg']));
    }
    else{
        $registerData['e-post'] = $_POST['e-post'];
        $registerData['passord'] = $_POST['passord'];
        $registerData['fornavn'] = $_POST['fornavn'];
        $registerData['etternavn'] = $_POST['etternavn'];

        $userAdded = $user->addUser($registerData);

        echo $twig->render('indexOppgave1.html', array('status' => 1, 'response' => $userAdded['message']));
    }
}